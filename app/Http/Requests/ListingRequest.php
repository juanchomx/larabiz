<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ListingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'address' => 'required',
            'website' => 'required',
            'email' => 'required|email',
            'phone' => 'required|integer',
            'bio' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'El campo name es requerido',
            'address.required' => 'El campo address es requerido',
            'website.required' => 'El campo website es requerido',
            'email.required' => 'El campo email es requerido',
            'phone.required' => 'El campo phone es requerido',
            'bio.required' => 'El campo bio es requerido',
        ];
    }

}
